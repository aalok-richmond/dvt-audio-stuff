for file in *.mp4
do
	ffmpeg -i "$file" -vn -acodec pcm_s16le -ac 1 "$(basename "$file" .mp4).wav"
done
