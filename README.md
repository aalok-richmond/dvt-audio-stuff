## The `pyAudioAnalysis' library in Python

* Installation:
    ```
        #   [dependencies]
        $   python3 -m pip install --user numpy matplotlib scipy sklearn hmmlearn simplejson eyed3 pydub
        
        #   [library]
        $   git clone https://github.com/tyiannak/pyAudioAnalysis.git
        $   cd pyAudioAnalysis
        $   python3 -m pip install --user -e .
        
    ```
* Additional packages:
    * `pysrt`
    ```
        #   [pysrt] python subtitle parser
        $   python3 -m pip install --user pysrt
        
        #   [pysrt] usage
        >   import pysrt
        >   subs = pysrt.open(filepath)
        >   num_subs = len(subs)
        >   subs[0].text, subs[0].start.seconds, subs[0].end.milliseconds
    ```

## The `SpeechRecognition' library in Python !! Turns out this is just a wrapper for multiple speech recog libraries

* Installation:
    ```
        $   python[3] -m pip install [--user] SpeechRecognition
    ```
* Usage:
    ```
        >   import speech_recognition [as sr]
    ```

## PocketSphinx stuff:

Useful intro doc courtesy of CMU Sphinx docpages:
    https://cmusphinx.github.io/wiki/tutorialconcepts/

Download CMU Sphinx `sphinx4` from https://sourceforge.net/projects/cmusphinx/

To compile, do:
```
    ./autogen.sh
    make
```


We might use the python package `pocketsphinx' for exploration.

To install, run:
```
        $   python[3] -m pip [--user] install pocketsphinx
            - [3]:      install specific to python3 (recommended)
            - [--user]: install without being root
```

Possible dependencies you may need to install (non-exhaustive list):
* libpulse-dev
* libasound2-dev
* build-essential
* swig
* python-dev

Dependencies you probably already have:
* python[3]
* python[3]-pip
* git

Usage:
```
    [python3]   import pocketsphinx
        OR
    [python3]   import pocketsphinx as ps
```
