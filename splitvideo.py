
# coding: utf-8

# In[1]:


import os
import pysrt
import sys


# In[ ]:


filepath = "../data/friends-s02-e03.mp4"
srtpath = "../data/friends-s02-e03.srt"


# In[3]:


basename = filepath.split('/')[-1].split('.')[0]
print(basename)


# In[ ]:


try:
    basename = sys.argv[1]
except IndexError:
    raise IOError("please pass file basename as command-line argument")


# In[4]:


subslist = pysrt.open(srtpath)
print(len(subslist))


# In[5]:


get_ipython().run_line_magic('mkdir', '-p ../data/clipped/$basename/')
# %mkdir -p ../data/clipped/$basename/mp4/
get_ipython().run_line_magic('mkdir', '-p ../data/clipped/$basename/ogg/')


# In[8]:


part = 1 # use 1-indexed for a change
for sub in subslist[:]:
    start = sub.start
    start = ['%02d'%i for i in [start.hours, start.minutes, start.seconds]] + ['%03d'%start.milliseconds]
    end = sub.end
    end = ['%02d'%i for i in [end.hours, end.minutes, end.seconds]] + ['%03d'%end.milliseconds]
#     print(start,end)
    os.system("ffmpeg " +              "-i %s "%(filepath,) +              "-ss %s:%s:%s.%s "%(*start,) +              "-to %s:%s:%s.%s "%(*end,) +              "-async 1 -c copy " +              "../data/clipped/%s/%03d.mp4"%(basename,part))
    os.system("ffmpeg " +              "-i %s "%("../data/clipped/%s/%03d.mp4"%(basename,part)) +              "-vn -acodec libvorbis " +              "../data/clipped/%s/ogg/%s.ogg"%(basename,'-'.join(start[-3:])+'_'+'-'.join(end[-3:])))
    part += 1
# %rm -rf ../data/$basename/mp4/

