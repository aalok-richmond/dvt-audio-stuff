#!/bin/bash
basename=$(echo "$1" | rev | cut -d"." -f2 | cut -d"/" -f1 | rev)
mkdir -p workingdir
ffmpeg -i $1 "workingdir/$basename.wav"
