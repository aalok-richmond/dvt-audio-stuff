
import numpy as np
import tensorflow as tf

import vggish_input
import vggish_slim
import vggish_postprocess

# load the wave file
input_wav = "/Users/taylor/local/dv/input/friends/friends-s02-e03.wav"
examples_batch = vggish_input.wavfile_to_examples(input_wav)

# load models and postprocessor (a PCA model)
pproc = vggish_postprocess.Postprocessor('vggish_pca_params.npz')
sess = tf.Session()
tf.Graph().as_default()
vggish_slim.define_vggish_slim(training=False)
vggish_slim.load_vggish_slim_checkpoint(sess, 'vggish_model.ckpt')
features_tensor = sess.graph.get_tensor_by_name('vggish/input_features:0')
embedding_tensor = sess.graph.get_tensor_by_name('vggish/embedding:0')

# Compute embeddings:
[embedding_batch] = sess.run([embedding_tensor],
                             feed_dict={features_tensor: examples_batch})
postprocessed_batch = pproc.postprocess(embedding_batch)

# Print out dimensions:
print("Shape of input batches: " + str(examples_batch.shape))
print("Shape of output from vggish: " + str(embedding_batch.shape))
print("Shape of output from postprocess: " + str(postprocessed_batch.shape))
