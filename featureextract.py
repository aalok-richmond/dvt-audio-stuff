
# coding: utf-8

# In[1]:


from pyAudioAnalysis import audioBasicIO
from pyAudioAnalysis import audioFeatureExtraction


# In[2]:


import pickle


# In[45]:


import numpy as np


# In[38]:


import os
import sys

# In[ ]:


try:
    basename = sys.argv[1]
except IndexError:
    print("please pass file basename as command-line argument")
    raise SystemExit


# In[3]:


#example
# [Fs, x] = audioBasicIO.readAudioFile("../data/clipped/friends-s02-e03/ogg/00-03-170_00-04-370.ogg")


# In[13]:


# print(Fs,x[:,1])


# In[ ]:


frame_width = 0.050 # millisec
frame_step = 0.025 # (millisec) this means 50% overlap between consecutive audio frames


# In[26]:


# F0, f_names = audioFeatureExtraction.stFeatureExtraction(x[:,0], Fs, frame_width*Fs, frame_step*Fs) # channel 0
# F1, _ = audioFeatureExtraction.stFeatureExtraction(x[:,1], Fs, frame_width*Fs, frame_step*Fs) # channel 1


# In[44]:


# n_features, n_frames = F0.shape
# print(F0.shape)


# In[66]:


ls = os.listdir("../data/clipped/{basename}/ogg/".format(basename=basename))
# print(ls)
get_ipython().run_line_magic('mkdir', '-p ../data/features/')
get_ipython().run_line_magic('mkdir', '-p ../data/features/$basename/')
i, n = 1, len(ls)
for filename in ls:
    print(basename, filename, "processing file {current}/{end}.".format(current=i,end=n), end='\r'); i+=1
    # read the frequency, and x-time-series
    [Fs, x] = audioBasicIO.readAudioFile("../data/clipped/{basename}/ogg/{filename}".format(basename=basename,filename=filename))
    F0, f_names = audioFeatureExtraction.stFeatureExtraction(x[:,0], Fs, frame_width*Fs, frame_step*Fs)
    F1, f_names = audioFeatureExtraction.stFeatureExtraction(x[:,1], Fs, frame_width*Fs, frame_step*Fs)
    F = np.array([*zip(F0.T, F1.T)]).reshape(1, -1) # merge channels and flatten
    with open("../data/features/{basename}/{filename}.f.pickle".format(basename=basename,filename=filename), 'wb') as out:
        pickle.dump(F, file=out)

